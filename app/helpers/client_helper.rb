module ClientHelper

  def link_for_edit(record)
    link_to('Edit', edit_server_path(record))
  end

end
