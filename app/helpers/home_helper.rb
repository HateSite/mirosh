module HomeHelper

  def link_for_download(record)
    if record.permission_download
      link_to('Download', client_path(record))
    end
  end

  def link_for_rename(record)
    if record.permission_rename
      link_to('Rename', client_path(record))
    end
  end

  def link_for_delete(record)
    if record.permission_delete
      link_to('Destroy', client_path(record),
              method: :delete,
              data: { confirm: "Are you sure you want to delete #{record.name} ?" })
    end
  end

end
