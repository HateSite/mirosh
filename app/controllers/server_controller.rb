class ServerController < ApplicationController

  def index
    @files = Upload.all
  end

  def edit
    @file = Upload.find(params[:id])
  end

  def update
    @file = Upload.find(params[:id])

    if @file.update(file_params)
      redirect_to "/server"
    end
  end

  private
    def file_params
      params.require(:upload).permit(
          :permission_download, :permission_rename, :permission_delete)
    end

end
