class ClientController < ApplicationController

  def index
    @files = Upload.all
  end

  def create
    @uploaded_io = params[:file]

    File.open(Rails.root.join('public', 'uploads', @uploaded_io.original_filename), 'wb') do |file|
      file.write(@uploaded_io.read)
    end

    @file = Upload.new({name: @uploaded_io.original_filename,
                        permission_download: true,
                        permission_rename: true,
                        permission_delete: true})
    @file.save
  end

  def destroy
    @file = Upload.find(params[:id])

    @path_to_file = "#{Rails.root}/public/uploads/#{@file.name}"
    File.delete(@path_to_file) if File.exist?(@path_to_file)

    @file.destroy

    redirect_to root_path
  end

  def show
    @file = Upload.find(params[:id])
    send_file "#{Rails.root}/public/uploads/#{@file.name}"
  end

end
