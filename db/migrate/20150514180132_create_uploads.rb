class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.string :name
      t.boolean :download
      t.boolean :rename
      t.boolean :destroy

      t.timestamps null: false
    end
  end
end
