class ChangeColumns < ActiveRecord::Migration
  def change
    change_table :uploads do |t|
      t.rename :download, :permission_download
      t.rename :rename, :permission_rename
      t.rename :destroy, :permission_delete
    end
  end
end
